export default function Test() {
  return (
    <>
      <div className="box">
        <div className="row header">
          <p><b>header</b>
            <br />
            <br />(sized to content)</p>
        </div>
        <div className="row content">
          <p>
            <b>content</b>
            (fills remaining space)
          </p>
        </div>
        <div className="row footer">
          <p><b>footer</b> (fixed height)</p>
        </div>
      </div>
      <style jsx>{`
        .box {
          display: flex;
          flex-flow: column;
          height: 100%;
        }

        .box .row {
          border: 1px dotted grey;
        }

        .box .row.header {
          flex: 0 1 auto;
              /* The above is shorthand for:
              flex-grow: 0,
              flex-shrink: 1,
              flex-basis: auto
              */
        }

        .box .row.content {
          flex: 1 1 auto;
        }

        .box .row.footer {
          flex: 0 1 40px;
        }
      `}</style>
    </>
  )
}

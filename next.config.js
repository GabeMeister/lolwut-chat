module.exports = {
  env: {
    PROD_SERVER_URL: 'https://lolwut-chat-server.herokuapp.com/',
    DEV_SERVER_URL: 'http://localhost:9000/'
  },
}

import { useRef } from 'react';

import styles from './ChatInput.module.css';

export default function ChatInput({ onSubmit }) {
  const inputRef = useRef(null);

  return (
    <>
      <input
        type="text"
        placeholder="Type something funny"
        className={styles.messageInput}
        onKeyPress={evt => {
          if (evt.key === 'Enter' && inputRef.current.value.length) {
            onSubmit(inputRef.current.value);
            inputRef.current.value = '';
          }
        }}
        ref={inputRef}
      />
    </>
  );
}

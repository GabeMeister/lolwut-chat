import styles from './ChatMessage.module.css';

export default function ChatMessage({ text }) {
  return (
    <div className={styles.chatMessage}>
      <span>{text}</span>
    </div>
  );
}

import { useState, useEffect } from 'react';
import io from 'socket.io-client';

import ChatInput from './ChatInput';
import ChatList from './ChatList';
import ChatMessage from './ChatMessage';

import styles from './ChatWindow.module.css';

const SERVER_URL = process.env.NODE_ENV === 'production'
  ? process.env.PROD_SERVER_URL
  : process.env.DEV_SERVER_URL;

const socket = io(SERVER_URL);
socket.on('connect', () => {
  console.log('connected');
});

export default function ChatWindow() {
  const [latestMsg, setLatestMsg] = useState(null);
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    // Handle new chat messages sent to us from the server
    socket.on('chat-message', msg => {
      console.log('received message', msg);
      setLatestMsg(msg);
    });
  }, []);

  // Add latest message to the message queue
  useEffect(() => {
    const messagesCopy = [...messages];
    if (latestMsg !== null) {
      messagesCopy.push(latestMsg);
    }
    setMessages(messagesCopy);
  }, [latestMsg]);

  // Scroll to the last chat message
  useEffect(() => {
    document.getElementById('hidden-last-msg').scrollIntoView();
  }, [messages]);

  const onMessageEntered = text => {
    if (socket.connected) {
      console.log('emitting message: ', text);
      socket.emit('chat-message', text);
    }
  };

  return (
    <div className={styles.chatWindow}>
      <ChatList>
        {messages.map(msg => {
          return (
            <ChatMessage key={msg.id} text={msg.text} />
          );
        })}
        <div id="hidden-last-msg" style={{ height: '0px' }}></div>
      </ChatList>
      <ChatInput onSubmit={onMessageEntered} />
    </div>
  );
}

import styles from './Headline.module.css';

export default function Headline({ children }) {
  return (
    <h1 className={styles.Headline}>{children}</h1>
  );
}

import styles from './ChatList.module.css';

export default function ChatList({ children }) {
  return (
    <div className={styles.chatList}>
      {children}
    </div>
  );
}

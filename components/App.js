import Head from 'next/head';

import ChatWindow from '../components/ChatWindow';

import styles from './App.module.css';

export default function App() {
  return (
    <div className={styles.App}>
      <Head>
        <title>lolwut</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <style jsx>{`
        @import url('https://fonts.googleapis.com/css2?family=Manrope&display=swap');
      `}</style>

      <ChatWindow />
    </div>
  );
}
